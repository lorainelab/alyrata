# What's here

* A_lyrata_Apr_2001.bed.gz - output of RenamingModels.Rmd, sorted and compressed using bgzip
* A_lyrata_Apr_2001.bed.gz.tbi - tabix index of A_lyrata_Apr_2001.bed.gz

To sort and index, did this:

    sort -k1,1 -k2,2n A_lyrata_Apr_2011.bed | bgzip > A_lyrata_Apr_2011.bed.gz
    tabix -s 1 -b 2 -e 3 A_lyrata_Apr_2011.bed.gz.tbi

* * *

# Questions? Comments?

Contact:

* Ann Loraine aloraine@uncc.edu

* * *

# License 

Copyright (c) 2015 University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT
